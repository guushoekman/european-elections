var none = {
  "name": "No European affiliation",
  "color": "#ccc",
}

var acre = {
  "name": "Alliance of Conservatives and Reformists in Europe",
  "color": "#0054A5",
}

var adde = {
  "name": "Alliance for Direct Democracy in Europe",
  "color": "#800080",
}

var edp = {
  "name": "European Democratic Party",
  "color": "#EE9900",
}

var pes = {
  "name": "Party of European Socialists",
  "color": "#F0001C",
}

var pel = {
  "name": "Party of the European Left",
  "color": "#aa0000",
}

var pirates = {
  "name": "European Pirate Party",
  "color": "#000000",
}

var initiative = {
  "name": "Initiative of Communist and Workers&#39; Parties",
  "color": "#f00",
}

var alde = {
  "name": "Alliance of Liberals and Democrats for Europe",
  "color": "#fac000",
}

var enl = {
  "name": "Movement for a Europe of Nations and Freedom",
  "color": "#2B3856",
}

var epp = {
  "name": "European People&#39;s Party",
  "color": "#3399FF",
}

var egp = {
  "name": "European Green Party",
  "color": "#009900",
}

var efa = {
  "name": "European Free Alliance",
  "color": "#662287",
}

var results = [
  {
    "country": "at",
    "countryName": "Austria",
    "totalSeats": 183,
    "totalVotes": 5120881,
    "latestElections": {
      "date": "15 October 2017",
      "url": "https://en.wikipedia.org/wiki/Austrian_legislative_election,_2017",
      "turnout": 80,
    },
    "topParties": [
      {
        "name": "ÖVP",
        "votes": 1595526,
        "seats": 62,
        "affiliation": epp,
      },
      {
        "name": "SPÖ",
        "votes": 1361746,
        "seats": 52,
        "affiliation": pes,
      },
      {
        "name": "FPÖ",
        "votes": 1316442,
        "seats": 51,
        "affiliation": enl,
      },
      {
        "name": "NEOS",
        "votes": 268518,
        "seats": 10,
        "affiliation": alde,
      },
      {
        "name": "PILZ",
        "votes": 223543,
        "seats": 8,
        "affiliation": none,
      },
    ]
  },
  {
    "country": "be",
    "countryName": "Belgium",
    "totalSeats": 150,
    "totalVotes": 7157498,
    "latestElections": {
      "date": "25 May 2014",
      "url": "https://en.wikipedia.org/wiki/Belgian_federal_election,_2014",
      "turnout": 89.5,
    },
    "topParties": [
      {
        "name": "N-VA",
        "votes": 1366414,
        "seats": 33,
        "affiliation": efa,
      },
      {
        "name": "PS",
        "votes": 787165,
        "seats": 23,
        "affiliation": pes,
      },
      {
        "name": "CD&V",
        "votes": 783060,
        "seats": 18,
        "affiliation": epp,
      },
      {
        "name": "Open Vld",
        "votes": 659582,
        "seats": 14,
        "affiliation": alde,
      },
      {
        "name": "MR",
        "votes": 650290,
        "seats": 20,
        "affiliation": alde,
        "repeat": true,
      },
    ]
  },
  {
    "country": "bg",
    "countryName": "Bulgaria",
    "totalSeats": 240,
    "totalVotes": 3682499,
    "latestElections": {
      "date": "26 March 2017",
      "url": "https://en.wikipedia.org/wiki/Bulgarian_parliamentary_election,_2017",
      "turnout": 54.1,
    },
    "topParties": [
      {
        "name": "ГЕРБ",
        "votes": 1147292,
        "seats": 95,
        "affiliation": epp,
      },
      {
        "name": "БСП за България",
        "votes": 955490,
        "seats": 80,
        "affiliation": pes,
      },
      {
        "name": "Обединени Патриоти",
        "votes": 318513,
        "seats": 27,
        "affiliation": acre,
      },
      {
        "name": "ДПС",
        "votes": 315976,
        "seats": 26,
        "affiliation": alde,
      },
      {
        "name": "Воля",
        "votes": 145637,
        "seats": 12,
        "affiliation": enl,
      },
    ]
  },
  {
    "country": "hr",
    "countryName": "Croatia",
    "totalSeats": 140,
    "totalVotes": 1919188,
    "latestElections": {
      "date": "11 September 2016",
      "url": "https://hr.wikipedia.org/wiki/Hrvatski_parlamentarni_izbori_2016.",
      "turnout": 54.4,
    },
    "topParties": [
      {
        "name": "HDZ",
        "votes": 682687,
        "seats": 61,
        "affiliation": epp,
      },
      {
        "name": "Narodna koalicija",
        "votes": 636602,
        "seats": 54,
        "affiliation": pes,
      },
      {
        "name": "Most",
        "votes": 186626,
        "seats": 13,
        "affiliation": none,
      },
      {
        "name": "Živi zid",
        "votes": 117208,
        "seats": 8,
        "affiliation": none,
        "repeat": true,
      },
      {
        "name": "IDS",
        "votes": 43180,
        "seats": 3,
        "affiliation": alde,
      },
    ]
  },
  {
    "country": "cy",
    "countryName": "Cyprus",
    "totalSeats": 56,
    "totalVotes": 362542,
    "latestElections": {
      "date": "22 May 2016",
      "url": "https://en.wikipedia.org/wiki/Cypriot_legislative_election,_2016",
      "turnout": 66.7,
    },
    "topParties": [
      {
        "name": "DISY",
        "votes": 107825,
        "seats": 18,
        "affiliation": epp,
      },
      {
        "name": "AKEL",
        "votes": 90204,
        "seats": 16,
        "affiliation": pel,
      },
      {
        "name": "DIKO",
        "votes": 50923,
        "seats": 9,
        "affiliation": none,
      },
      {
        "name": "EDEK",
        "votes": 21732,
        "seats": 3,
        "affiliation": pes,
      },
      {
        "name": "SYPOL",
        "votes": 21114,
        "seats": 3,
        "affiliation": edp,
      },
    ]
  },
  {
    "country": "cz",
    "countryName": "Czech Republic",
    "totalSeats": 200,
    "totalVotes": 5091065,
    "latestElections": {
      "date": "20 and 21 October 2017",
      "url": "https://en.wikipedia.org/wiki/Czech_legislative_election,_2017",
      "turnout": 60.8,
    },
    "topParties": [
      {
        "name": "ANO 2011",
        "votes": 1500113,
        "seats": 78,
        "affiliation": alde,
      },
      {
        "name": "ODS",
        "votes": 572962,
        "seats": 25,
        "affiliation": acre,
      },
      {
        "name": "Pirates",
        "votes": 546393,
        "seats": 22,
        "affiliation": pirates,
      },
      {
        "name": "SPD",
        "votes": 538574,
        "seats": 22,
        "affiliation": enl,
      },
      {
        "name": "KSČM",
        "votes": 393100,
        "seats": 15,
        "affiliation": pel,
      },
    ]
  },
  {
    "country": "dk",
    "countryName": "Denmark",
    "totalSeats": 179,
    "totalVotes": 3560060 + 23366 + 20514,
    "latestElections": {
      "date": "17 June 2015",
      "url": "https://en.wikipedia.org/wiki/Danish_general_election,_2015",
      "turnout": 85.8,
    },
    "topParties": [
      {
        "name": "Socialdemokratiet",
        "votes": 924940,
        "seats": 47,
        "affiliation": pes,
      },
      {
        "name": "Dansk Folkeparti",
        "votes": 741746,
        "seats": 37,
        "affiliation": none,
      },
      {
        "name": "Venstre",
        "votes": 685188,
        "seats": 34,
        "affiliation": alde,
      },
      {
        "name": "Enhedslisten – De Rød-Grønne",
        "votes": 274463,
        "seats": 14,
        "affiliation": pel,
      },
      {
        "name": "Liberal Alliance",
        "votes": 265129,
        "seats": 13,
        "affiliation": none,
      },
    ]
  },
  {
    "country": "ee",
    "countryName": "Estonia",
    "totalSeats": 101,
    "totalVotes": 577910,
    "latestElections": {
      "date": "1 March 2015",
      "url": "https://en.wikipedia.org/wiki/Estonian_parliamentary_election,_2015",
      "turnout": 64.2,
    },
    "topParties": [
      {
        "name": "Eesti Reformierakond",
        "votes": 158970,
        "seats": 30,
        "affiliation": alde,
      },
      {
        "name": "Eesti Keskerakond",
        "votes": 142458,
        "seats": 27,
        "affiliation": alde,
        "repeat": true,
      },
      {
        "name": "Sotsiaaldemokraatlik Erakond",
        "votes": 87189,
        "seats": 15,
        "affiliation": pes,
      },
      {
        "name": "Isamaa",
        "votes": 78699,
        "seats": 14,
        "affiliation": epp,
      },
      {
        "name": "Eesti Vabaerakond",
        "votes": 49882,
        "seats": 8,
        "affiliation": none,
      },
    ]
  },
  {
    "country": "fi",
    "countryName": "Finland",
    "totalSeats": 200,
    "totalVotes": 2983856,
    "latestElections": {
      "date": "19 April 2015",
      "url": "https://en.wikipedia.org/wiki/Finnish_parliamentary_election,_2015",
      "turnout": 70.1,
    },
    "topParties": [
      {
        "name": "Suomen Keskusta",
        "votes": 626218,
        "seats": 49,
        "affiliation": alde,
      },
      {
        "name": "Perussuomalaiset",
        "votes": 524054,
        "seats": 38,
        "affiliation": acre,
      },
      {
        "name": "Kansallinen Kokoomus",
        "votes": 540212,
        "seats": 37,
        "affiliation": epp,
      },
      {
        "name": "SDP",
        "votes": 490102,
        "seats": 34,
        "affiliation": pes,
      },
      {
        "name": "Vihreä liitto",
        "votes": 253102,
        "seats": 15,
        "affiliation": egp,
      },
    ]
  },
  {
    "country": "fr",
    "countryName": "France",
    "totalSeats": 577,
    "totalVotes": 20164615,
    "latestElections": {
      "date": "18 June 2017 - second round",
      "url": "https://en.wikipedia.org/wiki/French_legislative_election,_2017",
      "turnout": 42.6,
    },
    "topParties": [
      {
        "name": "REM",
        "votes": 7826245,
        "seats": 306,
        "affiliation": none,
      },
      {
        "name": "Les Républicains",
        "votes": 4040203,
        "seats": 112,
        "affiliation": epp,
      },
      {
        "name": "Rassemblement national",
        "votes": 1590869,
        "seats": 8,
        "affiliation": enl,
      },
      {
        "name": "MoDem",
        "votes": 1100656,
        "seats": 42,
        "affiliation": alde,
      },
      {
        "name": "Parti socialiste",
        "votes": 1032842,
        "seats": 30,
        "affiliation": pes,
      },
    ]
  },
  {
    "country": "de",
    "countryName": "Germany",
    "totalSeats": 709,
    "totalVotes": 46976341 + 46976341,
    "latestElections": {
      "date": "24 September 2017",
      "url": "https://en.wikipedia.org/wiki/German_federal_election,_2017",
      "turnout": 76.2,
    },
    "topParties": [
      {
        "name": "CDU",
        "votes": 14030751 + 12447656,
        "seats": 185 + 15,
        "affiliation": epp,
      },
      {
        "name": "SPD",
        "votes": 11429231 + 9539381,
        "seats": 59 + 94,
        "affiliation": pes,
      },
      {
        "name": "AfD",
        "votes": 5317499 + 5878115,
        "seats": 3 + 91,
        "affiliation": none,
      },
      {
        "name": "FDP",
        "votes": 3249238 + 4999449,
        "seats": 0 + 80,
        "affiliation": alde,
      },
      {
        "name": "DIE LINKE",
        "votes": 3966637 + 4297270,
        "seats": 5 + 64,
        "affiliation": pel,
      },
    ]
  },
  {
    "country": "gr",
    "countryName": "Greece",
    "totalSeats": 300,
    "totalVotes": 5566295,
    "latestElections": {
      "date": "20 September 2015",
      "url": "https://en.wikipedia.org/wiki/Greek_legislative_election,_September_2015",
      "turnout": 56.6,
    },
    "topParties": [
      {
        "name": "SYRIZA",
        "votes": 1926526,
        "seats": 145,
        "affiliation": pel,
      },
      {
        "name": "ΝΔ",
        "votes": 1526205,
        "seats": 75,
        "affiliation": epp,
      },
      {
        "name": "Golden Dawn",
        "votes": 379722,
        "seats": 18,
        "affiliation": none,
      },
      {
        "name": "ΔΗΣΥ",
        "votes": 341732,
        "seats": 17,
        "affiliation": none,
        "repeat": true,
      },
      {
        "name": "KKE",
        "votes": 301684,
        "seats": 15,
        "affiliation": initiative,
      },
    ]
  },
  {
    "country": "hu",
    "countryName": "Hungary",
    "totalSeats": 199,
    "totalVotes": 5732283 + 5504530,
    "latestElections": {
      "date": "8 April 2018",
      "url": "https://en.wikipedia.org/wiki/Hungarian_parliamentary_election,_2018",
      "turnout": 70.2,
    },
    "topParties": [
      {
        "name": "Fidesz–KDNP",
        "votes": 2824551 + 2636201,
        "seats": 42 + 91,
        "affiliation": epp,
      },
      {
        "name": "Jobbik",
        "votes": 1092806 + 1276840,
        "seats": 25 + 1,
        "affiliation": none,
      },
      {
        "name": "MSZP-P",
        "votes": 682701 + 622458,
        "seats": 12 + 8,
        "affiliation": none,
        "repeat": true,
      },
      {
        "name": "LMP",
        "votes": 404429 + 312731,
        "seats": 7 + 1,
        "affiliation": egp,
      },
      {
        "name": "DK",
        "votes": 308161 + 348176,
        "seats": 6 + 3,
        "affiliation": none,
      },
    ]
  },
  {
    "country": "it",
    "countryName": "Italy",
    "totalSeats": 630,
    "totalVotes": 33923321,
    "latestElections": {
      "date": "4 March 2018",
      "url": "https://en.wikipedia.org/wiki/Italian_general_election,_2018",
      "turnout": 72.9,
    },
    "topParties": [
      {
        "name": "M5S",
        "votes": 10732066 + 197346,
        "seats": 133 + 93 + 1,
        "affiliation": none,
      },
      {
        "name": "PD",
        "votes": 6161896 + 297153,
        "seats": 86 + 21 + 5,
        "affiliation": pes,
      },
      {
        "name": "Lega",
        "votes": 5698687 + 116680,
        "seats": 73 + 49 + 2,
        "affiliation": enl,
      },
      {
        "name": "Forza Italia",
        "votes": 4596956 + 94122,
        "seats": 59 + 46 + 1,
        "affiliation": epp,
      },
      {
        "name": "Fratelli d&#39;Italia",
        "votes": 1429550 + 29270,
        "seats": 19 + 12,
        "affiliation": none,
      },
    ]
  },
  {
    "country": "ie",
    "countryName": "Ireland",
    "totalSeats": 158,
    "totalVotes": 2151293,
    "latestElections": {
      "date": "26 February 2016",
      "url": "https://en.wikipedia.org/wiki/Irish_general_election,_2016",
      "turnout": 65.1,
    },
    "topParties": [
      {
        "name": "Fine Gael",
        "votes": 544140,
        "seats": 49,
        "affiliation": epp,
      },
      {
        "name": "Fianna Fáil",
        "votes": 519356,
        "seats": 44,
        "affiliation": alde,
      },
      {
        "name": "Sinn Féin",
        "votes": 295319,
        "seats": 23,
        "affiliation": none,
      },
      {
        "name": "Labour Party",
        "votes": 140898,
        "seats": 7,
        "affiliation": pes,
      },
      {
        "name": "AAA–PBP",
        "votes": 84168,
        "seats": 6,
        "affiliation": none,
      },
    ]
  },
  {
    "country": "lv",
    "countryName": "Latvia",
    "totalSeats": 100,
    "totalVotes": 913491,
    "latestElections": {
      "date": "4 October 2014",
      "url": "https://en.wikipedia.org/wiki/Latvian_parliamentary_election,_2014",
      "turnout": 58.9,
    },
    "topParties": [
      {
        "name": "SDPS",
        "votes": 209887,
        "seats": 24,
        "affiliation": pes,
      },
      {
        "name": "Vienotība",
        "votes": 199535,
        "seats": 23,
        "affiliation": epp,
      },
      {
        "name": "ZZS",
        "votes": 178210,
        "seats": 21,
        "affiliation": egp,
      },
      {
        "name": "NA",
        "votes": 151567,
        "seats": 17,
        "affiliation": acre,
      },
      {
        "name": "No sirds Latvijai",
        "votes": 62521,
        "seats": 7,
        "affiliation": none,
      },
    ]
  },
  {
    "country": "lt",
    "countryName": "Lithuania",
    "totalSeats": 141,
    "totalVotes": 1273427 + 1272734 + 913752,
    "latestElections": {
      "date": "9 and 23 October 2016",
      "url": "https://en.wikipedia.org/wiki/Lithuanian_parliamentary_election,_2016",
      "turnout": 46.5,
    },
    "topParties": [
      {
        "name": "LVŽS",
        "votes": 274108 + 229769 + 311611,
        "seats": 19 + 35,
        "affiliation": egp,
      },
      {
        "name": "TS-LKD",
        "votes": 276275 + 258834 + 246108,
        "seats": 20 + 1 + 10,
        "affiliation": epp,
      },
      {
        "name": "LSDP",
        "votes": 183597 + 183267 + 115599,
        "seats": 13 + 4,
        "affiliation": pes,
      },
      {
        "name": "LRLS",
        "votes": 115361 + 139522 + 70055,
        "seats": 8 + 6,
        "affiliation": alde,
      },
      {
        "name": "LRLS",
        "votes": 67817 + 70958 + 28894,
        "seats": 5 + 3,
        "affiliation": adde,
      },
    ]
  },
  {
    "country": "nl",
    "countryName": "Netherlands",
    "totalSeats": 150,
    "totalVotes": 10516041,
    "latestElections": {
      "date": "15 March 2017",
      "url": "https://en.wikipedia.org/wiki/Dutch_general_election,_2017",
      "turnout": 81.9,
    },
    "topParties": [
      {
        "name": "VVD",
        "votes": 2238351,
        "seats": 33,
        "affiliation": alde,
      },
      {
        "name": "PVV",
        "votes": 1372941,
        "seats": 20,
        "affiliation": enl,
      },
      {
        "name": "CDA",
        "votes": 1301796,
        "seats": 19,
        "affiliation": epp,
      },
      {
        "name": "D66",
        "votes": 1285819,
        "seats": 19,
        "affiliation": alde,
      },
      {
        "name": "GroenLinks",
        "votes": 959600,
        "seats": 14,
        "affiliation": egp,
      },
    ]
  },
  {
    "country": "pt",
    "countryName": "Portugal",
    "totalSeats": 230,
    "totalVotes": 5408092,
    "latestElections": {
      "date": "4 October 2015",
      "url": "https://en.wikipedia.org/wiki/Portuguese_legislative_election,_2015",
      "turnout": 55.8,
    },
    "topParties": [
      {
        "name": "PàF",
        "votes": 1993504,
        "seats": 102,
        "affiliation": epp,
      },
      {
        "name": "PS",
        "votes": 1747730,
        "seats": 86,
        "affiliation": pes,
      },
      {
        "name": "Bloco de Esquerda",
        "votes": 550945,
        "seats": 19,
        "affiliation": pel,
      },
      {
        "name": "PCP–PEV",
        "votes": 445901,
        "seats": 17,
        "affiliation": none,
      },
      {
        "name": "PSD",
        "votes": 80841,
        "seats": 5,
        "affiliation": epp,
      },
    ]
  },
  {
    "country": "es",
    "countryName": "Spain",
    "totalSeats": 350,
    "totalVotes": 24279259,
    "latestElections": {
      "date": "26 June 2016",
      "url": "https://en.wikipedia.org/wiki/Spanish_general_election,_2016",
      "turnout": 66.5,
    },
    "topParties": [
      {
        "name": "PP",
        "votes": 7941236,
        "seats": 137,
        "affiliation": epp,
      },
      {
        "name": "PSOE",
        "votes": 5443846,
        "seats": 85,
        "affiliation": pes,
      },
      {
        "name": "PODEMOS-IU-EQUO",
        "votes": 3227123,
        "seats": 45,
        "affiliation": none,
      },
      {
        "name": "C&#39;s",
        "votes": 3141570,
        "seats": 32,
        "affiliation": none,
        "repeat": true,
      },
      {
        "name": "EN COMÚ",
        "votes": 853102,
        "seats": 12,
        "affiliation": alde,
      },
    ]
  },
  {
    "country": "gb",
    "countryName": "United Kingdom",
    "totalSeats": 650,
    "totalVotes": 32204124,
    "latestElections": {
      "date": "8 June 2017",
      "url": "https://en.wikipedia.org/wiki/United_Kingdom_general_election,_2017",
      "turnout": 68.8,
    },
    "topParties": [
      {
        "name": "Conservative Party",
        "votes": 13636684,
        "seats": 317,
        "affiliation": acre,
      },
      {
        "name": "Labour Party",
        "votes": 12878460,
        "seats": 262,
        "affiliation": pes,
      },
      {
        "name": "Liberal Democrats",
        "votes": 2371910,
        "seats": 12,
        "affiliation": alde,
      },
      {
        "name": "Scottish National Party",
        "votes": 977569,
        "seats": 35,
        "affiliation": efa,
      },
      {
        "name": "Green Party of England and Wales",
        "votes": 525435,
        "seats": 1,
        "affiliation": egp,
      },
    ]
  },
]