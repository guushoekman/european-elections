window.onload = function () {
  $.each(results, function() {
    $("#bar-charts").append("<div class='single-country " + this.country + "'><h2 class='title is-3'>" + this.countryName + " <small>(last elections held " + this.latestElections.date + " | <a target='_blank' href='" + this.latestElections.url + "'>source</a>)</small></h2><h3 class='title is-5'>Votes <small>(" + this.latestElections.turnout + "% turnout)</small></h3><div class='votes-chart'></div><h3 class='title is-5'>Seats <small>(" + this.totalSeats + ")</small></h3><div class='seats-chart'></div></div>")

    var votesChart = $(".single-country." + this.country + " .votes-chart");
    var seatsChart = $(".single-country." + this.country + " .seats-chart");
    var totalVotes = this.totalVotes;
    var totalSeats = this.totalSeats;

    $(votesChart).append( $("<span class='country-code'>" + this.country + "</span>") )

    $.each(this.topParties, function() {
      var votesPercentage = this.votes / totalVotes * 100;
      var seatsPercentage = this.seats / totalSeats * 100;

      if (this.repeat == true) {
        borderWidth = 1;
      } else {
        var borderWidth = 0;
      }

      var votesLabel = votesPercentage.toFixed(1) + "%";
      if (votesPercentage < 4) {
        votesLabel = "&#8203;";
      }

      var seatsLabel = this.seats;
      if (seatsPercentage < 4) {
        seatsLabel = "&#8203;";
      }

      $(votesChart).append( $("<span class='bar tooltip' data-tooltip='" + this.name + " / " + this.affiliation.name + "'>" + votesLabel + "</span>").css({ "background-color": this.affiliation.color, "border-left": borderWidth + "px solid #fff" }).width(votesPercentage + "%") )
      $(seatsChart).append( $("<span class='bar tooltip is-tooltip-bottom' data-tooltip='" + this.name + " / " + this.affiliation.name + "'>" + seatsLabel + "</span>").css({ "background-color": this.affiliation.color, "border-left": borderWidth + "px solid #fff" }).width(seatsPercentage + "%") )
    })
  })
}